# Bienvenue à l'atelier pratique Git pour étudiants de IAI

- Date : 30 Juillet 2017
- Heure : 16 h
- Lieu : Institut Africain d'Informatique
- Par : Koffi Sani (@koffisani)

## Pré-requis
Pendant que vous êtes là, 
- Créer un compte sur la page [gitlab.com](https://gitlab.com);

## Ressources

- Site web de Git : [https://git-scm.com](https://git-scm.com);
- Site de GitHub : [https://github.com](https://github.com)
- Site web de Gitlab : [https://gitlab.com](https://gitlab.com)

Aussi:

- Git cheatsheet (dans [le répertoire ressources](ressources/github-git-cheat-sheet-FR.pdf))
- Markdown cheatsheet (dans [le répertoire ressources](ressources/markdown-cheatsheet-online.pdf))